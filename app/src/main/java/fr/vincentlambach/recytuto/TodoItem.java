package fr.vincentlambach.recytuto;

import android.annotation.SuppressLint;
import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TodoItem {

    @SuppressLint("SimpleDateFormat")
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static long compte;

    public enum Tags {
        Faible("Faible"), Normal("Normal"), Important("Important");

        private String desc;

        Tags(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    private long id;

    private long order;

    private String label;
    private Tags tag;
    private String dateExpiration;
    private boolean done;

    private boolean moved;

    public TodoItem(Tags tag, String label) {
        this.tag = tag;
        this.label = label;
        this.done = false;

        this.order = compte++;
    }

    public TodoItem(Tags tag, String label, Date dateExpiration) {
        this.tag = tag;
        this.label = label;
        this.dateExpiration = dateFormat.format(dateExpiration);
        this.done = false;

        this.order = compte++;
    }

    public TodoItem(Tags tag, String label, long order) {
        this.tag = tag;
        this.label = label;
        this.done = false;

        this.order = order;
    }

    public TodoItem(String label, Tags tag, boolean done) {
        this.label = label;
        this.tag = tag;
        this.done = done;

        this.order = compte++;
    }

    public TodoItem(String label, Tags tag, boolean done, long order, String dateExpiration) {
        this.label = label;
        this.tag = tag;
        this.dateExpiration = dateExpiration;

        this.done = done;
        this.order = order;
    }

    public static Tags getTagFor(String desc) {
        for (Tags tag : Tags.values()) {
            if (desc.compareTo(tag.getDesc()) == 0)
                return tag;
        }

        return Tags.Faible;
    }

    public String getLabel() {
        return label;
    }

    public Tags getTag() {
        return tag;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean getDone() {
        return done;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTag(Tags tag) {
        this.tag = tag;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getId() {
        return id;
    }

    public static void addItem(TodoItem item, Context context) {
        long id = TodoDbHelper.addItem(item, context);
        item.setId(id);
    }

    public long getOrder() {
        return order;
    }

    public void setOrder(long order) {
        this.order = order;
    }

    public boolean isMoved() {
        return moved;
    }

    public void setMoved(boolean moved) {
        this.moved = moved;
    }

    // Utilisé pour le debug avec les Logs
    @Override
    public String toString() {
        return "id = " + id + ", label : " + label + ", tag : " + tag + ", done : " + done + ", order : " + order;
    }

    public String getDateExpiration() {
        return dateExpiration;
    }
}
