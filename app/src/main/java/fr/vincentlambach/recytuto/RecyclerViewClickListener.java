package fr.vincentlambach.recytuto;

import android.view.View;

public interface RecyclerViewClickListener {
    public void recyclerViewListClicked(View v, int position, boolean longClick);
}