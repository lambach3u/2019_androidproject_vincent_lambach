package fr.vincentlambach.recytuto;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.TodoHolder> {

    private List<TodoItem> items;
    private static RecyclerViewClickListener listener;

    public RecyclerAdapter(List<TodoItem> items, RecyclerViewClickListener itemListener) {
        this.items = items;
        listener = itemListener;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new TodoHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, int position) {
        TodoItem it = items.get(position);
        holder.bindTodo(it);
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class TodoHolder extends ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private View view;

        private Resources resources;
        private ImageView image;
        private Switch sw;
        private TextView label;
        private TextView dateExpiration;
        private TodoItem item;

        public TodoHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.image = itemView.findViewById(R.id.imageView);
            this.sw = itemView.findViewById(R.id.switch1);
            this.label = itemView.findViewById(R.id.textView);
            this.resources = itemView.getResources();
            this.dateExpiration = itemView.findViewById(R.id.dateExpirationRow);


            ImageView imageView = itemView.findViewById(R.id.imageClock);
            imageView.setImageResource(R.drawable.clock_layer);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @SuppressLint("SimpleDateFormat")
        public void bindTodo(TodoItem todo) {
            this.item = todo;
            this.label.setText(todo.getLabel());
            this.sw.setChecked(todo.isDone());

            try {
                this.dateExpiration.setText(new SimpleDateFormat("dd/MM/yyyy").format(TodoItem.dateFormat.parse(todo.getDateExpiration())));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (todo.isDone())
                this.view.setBackgroundColor(this.resources.getColor(R.color.done));
            else
                this.view.setBackgroundColor(this.resources.getColor(R.color.defaultcolor));

            switch (todo.getTag()) {
                case Faible:
                    this.image.setBackgroundColor(this.resources.getColor(R.color.faible));
                    break;
                case Normal:
                    this.image.setBackgroundColor(this.resources.getColor(R.color.normal));
                    break;
                case Important:
                    this.image.setBackgroundColor(this.resources.getColor(R.color.important));
                    break;
            }

            TodoDbHelper.update(todo, this.view.getContext());
        }

        @Override
        public boolean onLongClick(View v) {
            listener.recyclerViewListClicked(v, this.getLayoutPosition(), true);
            return true;
        }

        @Override
        public void onClick(View v) {
            listener.recyclerViewListClicked(v, this.getLayoutPosition(), false);
        }
    }
}
