package fr.vincentlambach.recytuto;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements RecyclerViewClickListener {

    private List<TodoItem> items;
    private RecyclerView recycler;
    private LinearLayoutManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        Toolbar toolbar = this.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        this.setSupportActionBar(toolbar);

        FloatingActionButton fab = this.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), AddTaskActivity.class);
                startActivity(myIntent);
                finish();
            }
        });
        Log.i("INIT", "Fin initialisation composantes");

        // Test d'ajout d'un item
//        TodoItem item = new TodoItem(TodoItem.Tags.Important, "Réviser ses cours");
//        TodoItem.addItem(item, getBaseContext());
//        item = new TodoItem(TodoItem.Tags.Normal, "Acheter du pain");
//        TodoItem.addItem(item, getBaseContext());

        this.items = TodoDbHelper.getItems(getBaseContext());
        this.sortItems();

        Log.i("INIT", "Fin initialisation items");

        this.recycler = this.findViewById(R.id.recycler);
        this.manager = new LinearLayoutManager(this);
        this.recycler.setLayoutManager(this.manager);

        this.removeOutdatedItems();
        this.refresh();

        this.setRecyclerViewItemTouchListener();
        Log.i("INIT", "Fin initialisation recycler");

        Intent notifyIntent = new Intent(this, MyReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast
                (getApplicationContext(), 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 11);
        calendar.set(Calendar.MINUTE, 57);
        calendar.set(Calendar.SECOND, 0);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }


    private void removeOutdatedItems() {
        Date now = new Date();

        boolean changed = false;
        try {
            Date date = TodoItem.dateFormat.parse(TodoItem.dateFormat.format(now));
            for (TodoItem todoItem : this.items) {
                Date todoDate = TodoItem.dateFormat.parse(todoItem.getDateExpiration());
                if (todoDate.before(date)) {
                    TodoDbHelper.removeItem(todoItem, this.getBaseContext());
                    changed = true;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (changed)
            this.items = TodoDbHelper.getItems(getBaseContext());
    }

    private void sortItems() {
        if (this.items == null || this.items.size() < 2)
            return;

        Collections.sort(this.items, new Comparator<TodoItem>() {
            @Override
            public int compare(TodoItem o1, TodoItem o2) {
                if (o1.getOrder() < o2.getOrder())
                    return -1;
                else if (o1.getOrder() > o2.getOrder())
                    return 1;
                return 0;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.items = TodoDbHelper.getItems(getBaseContext());

        this.removeOutdatedItems();
        this.refresh();
        this.sortItems();
    }

    private void refresh() {
        this.recycler.setAdapter(new RecyclerAdapter(this.items, this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_debug:
                Intent dbmanager = new Intent(this.getApplicationContext(), AndroidDatabaseManager.class);
                this.startActivity(dbmanager);
                break;
            case R.id.action_empty:
                TodoDbHelper.emptyBase(this.getApplicationContext());

                this.items = new ArrayList<>();
                this.refresh();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setRecyclerViewItemTouchListener() {
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder source, RecyclerView.ViewHolder target) {
                TodoItem sourceItem = items.get(source.getAdapterPosition());
                sourceItem.setMoved(true);
                long sourceOrder = sourceItem.getOrder();

                TodoItem targetItem = items.get(target.getAdapterPosition());
                long targetOrder = targetItem.getOrder();

                Log.w("avant", items.toString());
                if (target.getAdapterPosition() > source.getAdapterPosition()) {
                    items.remove(target.getAdapterPosition());
                    items.remove(source.getAdapterPosition());
                } else {
                    items.remove(source.getAdapterPosition());
                    items.remove(target.getAdapterPosition());
                }

                targetItem.setOrder(sourceOrder);
                sourceItem.setOrder(targetOrder);
                items.add(sourceItem);
                items.add(targetItem);

                sortItems();
                refresh();
                Log.w("après", items.toString());

                Objects.requireNonNull(recycler.getAdapter()).notifyItemChanged(source.getAdapterPosition());
                Objects.requireNonNull(recycler.getAdapter()).notifyItemChanged(target.getAdapterPosition());

                return true;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                TodoItem item = items.get(position);

                switch (swipeDir) {
                    case ItemTouchHelper.RIGHT:
                        item.setDone(true);
                        break;
                    case ItemTouchHelper.LEFT:
                        item.setDone(false);
                        break;
                }
                Objects.requireNonNull(recycler.getAdapter()).notifyItemChanged(position);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(this.recycler);
    }

    public void removeItem(int position) {
        TodoDbHelper.removeItem(this.items.get(position), this.getBaseContext());
        this.items.remove(position);
        this.refresh();
    }

    @Override
    public void recyclerViewListClicked(View v, int position, boolean longClick) {
        TodoItem item = this.items.get(position);
        if (longClick) {
            if (item.isMoved()) // TODO: 17/03/2019 supprimer
                return;

            final int pos = position;

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert).setTitle("Suppression de l'item")
                    .setMessage("Êtes-vous sûrs de vouloir supprimer cet item ?")
                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            removeItem(pos);
                        }

                    }).setNegativeButton("Non", null).show();
        } else {
            Intent myIntent = new Intent(getApplicationContext(), AddTaskActivity.class);

            myIntent.putExtra("labelText", item.getLabel());
            myIntent.putExtra("tagDesc", item.getTag().getDesc());
            myIntent.putExtra("idItem", item.getId());
            myIntent.putExtra("dateExpiration", item.getDateExpiration());
            startActivity(myIntent);
        }
    }
}
