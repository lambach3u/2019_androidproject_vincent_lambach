package fr.vincentlambach.recytuto;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import fr.vincentlambach.recytuto.TodoItem.Tags;

public class AddTaskActivity extends AppCompatActivity implements OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        boolean editing = false;

        Bundle extras = getIntent().getExtras();
        long idItem = -1;

        DatePicker datePicker = findViewById(R.id.dateExpiration);
        datePicker.setMinDate(System.currentTimeMillis() - 1000);

        if (extras != null) {
            editing = true;

            String label = extras.getString("labelText");
            EditText editText = findViewById(R.id.nom);
            editText.setText(label);

            Tags tag = TodoItem.getTagFor(extras.getString("tagDesc"));
            idItem = extras.getLong("idItem");

            String dateExpiration = extras.getString("dateExpiration");
            try {
                @SuppressLint("SimpleDateFormat") Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateExpiration);
                Log.w("dateAvant", dateExpiration);
                Log.w("dateApres", date.toString());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            int id = 0;
            switch (tag) {
                case Normal:
                    id = R.id.normal;
                    break;
                case Important:
                    id = R.id.important;
                    break;
                default:
                    id = R.id.faible;
                    break;
            }
            RadioButton radioButton = findViewById(id);
            radioButton.setChecked(true);
        }

        Button valider = findViewById(R.id.validation);

        final boolean finalEditing = editing;
        final long finalIdItem = idItem;

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText label = findViewById(R.id.nom);
                String labelText = label.getText().toString();

                RadioGroup radioGroup = findViewById(R.id.tag);
                int radioButtonID = radioGroup.getCheckedRadioButtonId();
                View radioButton = radioGroup.findViewById(radioButtonID);
                int idx = radioGroup.indexOfChild(radioButton);
                RadioButton r = (RadioButton) radioGroup.getChildAt(idx);

                DatePicker datePicker = findViewById(R.id.dateExpiration);
                int year = datePicker.getYear();
                int month = datePicker.getMonth();
                int day = datePicker.getDayOfMonth();
                Calendar calendar = Calendar.getInstance();
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);
                Date date = calendar.getTime();

                Tags tag = null;
                switch (r.getId()) {
                    case R.id.faible:
                        tag = Tags.Faible;
                        break;
                    case R.id.normal:
                        tag = Tags.Normal;
                        break;
                    case R.id.important:
                        tag = Tags.Important;
                        break;
                }

                if (!labelText.equals("")) {
                    if (!finalEditing) {
                        TodoItem item = new TodoItem(tag, labelText, date);
                        TodoItem.addItem(item, getApplicationContext());
                    } else {
                        TodoItem item = new TodoItem(tag, labelText, date);
                        item.setId(finalIdItem);
                        TodoDbHelper.update(item, getApplicationContext());
                    }

                    Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivityIfNeeded(myIntent, 0);
                    finish();
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
