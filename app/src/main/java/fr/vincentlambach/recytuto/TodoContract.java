package fr.vincentlambach.recytuto;

import android.provider.BaseColumns;


public final class TodoContract {
    public static class TodoEntry implements BaseColumns {
        public static final String TABLE_NAME = "items";
        public static final String COLUMN_NAME_LABEL = "label";
        public static final String COLUMN_NAME_TAG = "tag";
        public static final String COLUMN_NAME_DONE = "done";
        public static final String COLUMN_NAME_DATE_EXPIRATION = "dateExpiration";
        public static final String COLUMN_NAME_ORDERID = "orderid";
    }
}
