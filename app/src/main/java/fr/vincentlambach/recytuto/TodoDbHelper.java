package fr.vincentlambach.recytuto;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class TodoDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "todo.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TodoContract.TodoEntry.TABLE_NAME + " (" +
                    TodoContract.TodoEntry._ID + " INTEGER PRIMARY KEY," +
                    TodoContract.TodoEntry.COLUMN_NAME_LABEL + " TEXT," +
                    TodoContract.TodoEntry.COLUMN_NAME_TAG + " TEXT," +
                    TodoContract.TodoEntry.COLUMN_NAME_ORDERID + " INTEGER," +
                    TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPIRATION + " DATE," +
                    TodoContract.TodoEntry.COLUMN_NAME_DONE + " INTEGER)";

    public TodoDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static void emptyBase(Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        db.delete(TodoContract.TodoEntry.TABLE_NAME, "", new String[0]);

        dbHelper.close();
    }

    public static void removeItem(TodoItem todoItem, Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long id = todoItem.getId();
        if (id != 0)
            db.delete(TodoContract.TodoEntry.TABLE_NAME, TodoContract.TodoEntry._ID + " = ?", new String[]{String.valueOf(id)});

        dbHelper.close();
    }

    public static void update(TodoItem todoItem, Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long id = todoItem.getId();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TodoContract.TodoEntry.COLUMN_NAME_DONE, todoItem.getDone());
        contentValues.put(TodoContract.TodoEntry.COLUMN_NAME_LABEL, todoItem.getLabel());
        contentValues.put(TodoContract.TodoEntry.COLUMN_NAME_TAG, todoItem.getTag().getDesc());
        contentValues.put(TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPIRATION, todoItem.getDateExpiration());
        contentValues.put(TodoContract.TodoEntry.COLUMN_NAME_ORDERID, todoItem.getOrder());

        if (id != 0)
            db.update(TodoContract.TodoEntry.TABLE_NAME, contentValues, TodoContract.TodoEntry._ID + " = ?", new String[]{String.valueOf(id)});

        dbHelper.close();

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    static List<TodoItem> getItems(Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                TodoContract.TodoEntry._ID,
                TodoContract.TodoEntry.COLUMN_NAME_LABEL,
                TodoContract.TodoEntry.COLUMN_NAME_TAG,
                TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPIRATION,
                TodoContract.TodoEntry.COLUMN_NAME_ORDERID,
                TodoContract.TodoEntry.COLUMN_NAME_DONE
        };

        Cursor cursor = db.query(
                TodoContract.TodoEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        List<TodoItem> items = new ArrayList<>();

        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndex(TodoContract.TodoEntry._ID));
            String label = cursor.getString(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_LABEL));
            TodoItem.Tags tag = TodoItem.getTagFor(cursor.getString(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_TAG)));
            String date = cursor.getString(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPIRATION));
            boolean done = (cursor.getInt(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_DONE)) == 1);
            long order = cursor.getLong(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_ORDERID));

            TodoItem item = new TodoItem(label, tag, done, order, date);
            item.setId(id);
            items.add(item);
        }

        dbHelper.close();

        return items;
    }

    static long addItem(TodoItem item, Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        ContentValues values = new ContentValues();
        values.put(TodoContract.TodoEntry.COLUMN_NAME_LABEL, item.getLabel());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_TAG, item.getTag().getDesc());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPIRATION, item.getDateExpiration());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_DONE, item.isDone());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_ORDERID, item.getOrder());

        long newRowId = db.insert(TodoContract.TodoEntry.TABLE_NAME, null, values);

        dbHelper.close();

        return newRowId;
    }


    public ArrayList<Cursor> getData(String Query) {
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[]{"message"};

        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try {
            String maxQuery = Query;

            Cursor c = sqlDB.rawQuery(maxQuery, null);

            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
        } catch (SQLException sqlEx) {
            Log.d("printing exception", sqlEx.getMessage());

            Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        } catch (Exception ex) {
            Log.d("printing exception", ex.getMessage());

            Cursor2.addRow(new Object[]{"" + ex.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }
    }
}
